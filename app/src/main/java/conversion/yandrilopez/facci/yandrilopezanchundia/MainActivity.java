package conversion.yandrilopez.facci.yandrilopezanchundia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

public class MainActivity extends AppCompatActivity  {

    EditText temperatura;
    Button  conversion;
    RadioButton celciAfare, fareAcelci;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        temperatura = (EditText)findViewById(R.id.valor);

        conversion = (Button)findViewById(R.id.convertir);
        celciAfare = (RadioButton)findViewById(R.id.celciAfare);
        fareAcelci = (RadioButton)findViewById(R.id.fareAcelci);

        conversion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double temp = new Double(temperatura.getText().toString());
                if (fareAcelci.isChecked())
                    temp = Convertir.CelsiusAFare(temp);
                else temp = Convertir.FareACelsius(temp);
                temperatura.setText(new Double(temp).toString());
            }
        });
    }




}
